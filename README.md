# Desafio

A proposta é criar um banner de 728x90px que foi desenvolvido originariamente em FLASH.
O Google lançou uma campanha para promover o Google Engage, criando um banner em FLASH que fornecia um link para visitar a home daquele projeto.

# A missão

Sua missão é criar um banner semelhante a esse (mostrado no arquivo original.html). O banner possui quatro quadros e foi desenvolvido para um ciclo apenas. Ao abrir o arquivo que mostra o banner, recarregue a página para acionar o ciclo único do banner. No item seguinte fornecemos imagens dos quatro quadros, (sem animações, seu trabalho deverá ser anima-lo)

# Imagens orientativas

Com o objetivo de facilitar seu trabalho de tomada de screenshots do banner fornecemos a seguir uma imagem orientativa dos quadros do banner. Clicando sobre a imagem você poderá visualizá-la em tamanho aumentado.

![Quadros](quadros.jpg)

# Cores e fontes

Com o objetivo de servir como referência, não há obrigatoriedade de uso, as cores e fontes que você poderá usar para desenvolver seu trabalho são as seguintes:

# Cores

A paleta de cores, além das cores preta, branca e tons de cinza, é mostrada a seguir:

- <span style="background-color: #0e4c7c; padding: 5px 10px;">#0E4C7C</span>
- <span style="background-color: #3399cc; padding: 5px 10px;">#3399CC</span>
- <span style="background-color: #63b9d7; padding: 5px 10px;">#63B9D7</span>
- <span style="background-color: #ff9900; padding: 5px 10px;">#FF9900</span>
- <span style="background-color: #fcc547; padding: 5px 10px;">#FCC547</span>
- <span style="background-color: #ea325b; padding: 5px 10px;">#EA325B</span>
- <span style="background-color: #56ab52; padding: 5px 10px;">#56AB52</span>

# Fontes

Você tem duas opções para uso das fontes no seu trabalho:

## Opção 1
Usar três famílias de fontes:
Fonte: ‘Source Sans Pro’ — Use para todos os textos exceto indicado a seguir.
Fonte: ‘Bowlby One SC’ — Use na palavra ADWORDS colorida mostrada no quadro 2.
Para inserir essas duas fontes no seu trabalho, importe-as do [Google Fonts][3045d920] inserindo na seção HEAD do documento um link, conforme instruções naquela página.
Fonte genérica serif: Use na palavra Google colorida mostrada no quadro 4.

  [3045d920]: https://www.google.com/fonts/ "Google Fonts"

## Opção 2
Usar fontes das famílias genéricas serif e sans-serif, conforme sua escolha.

Nota: Não use fontes que não sejas aquelas indicadas nas duas opções anteriores. Escolha uma das opções e siga com ela.

# Regras gerais
O objetivo do desafio é criar um banner nas dimensões 728x90px, usando somente HTML5 e CSS, o mais semelhante possível ao banner original. Não se trata de reprodução “pixel perfect” ou mesmo “animations perfect”. Dê o máximo de si, sem se estressar e terá concluído seu trabalho.
Seu trabalho será válido ainda que um efeito ou animação tenha ficado diferente ou somente parecido com o original. Procure chegar o mais próximo possível do original.

1. Nâo use JavaScript ou imagens de qualquer tipo ou natureza.

0. O único link admitido na seção HEAD do documento é para as fontes do Google, conforme explicado no item anterior.

0. A marcação HTML5 é livre, mas deverá obrigatoriamente validar no W3C.

0. As CSS não precisam validar.

0. Não use a tag a na marcação. Crie o botão do quadro 4 com o elemento button sem links.

0. Use somente as fontes conforme as opções 1 e 2 anteriores.

0. Faça um fork do projeto e submeta um pull request com o seu código.

    1. Se não tiver conhecimento de GIT, seu trabalho deve ser enviado por email para: ti@happygoat.com.br assunto escreva: Vaga de Estágio. Envie somente um arquivo .html (com as CSS incorporadas ao arquivo) não zipado e nomeado com o seu nome, separe nome e sobrenomes com hífen (por exemplo: jose-maria-da-silva.html).
